const { expect } = require("chai");
const { ethers } = require("hardhat");

const type = { Params: [{ name: "i", type: "uint256" }] };

describe("RecoverTest", () => {
  let signer;
  let test;
  let sig;
  let sigSplitted;
  let chainId;

  before(async () => {
    [signer] = await ethers.getSigners();
    chainId = (await ethers.provider.getNetwork()).chainId;
  });

  beforeEach(async () => {
    test = await (await ethers.getContractFactory("RecoverTest")).deploy();
    await test.waitForDeployment();

    sig = await signer.signTypedData(
      getDomain(chainId, await test.getAddress()),
      type,
      {
        i: 1,
      }
    );
    sigSplitted = {
      r: sig.slice(0, 66),
      s: "0x" + sig.slice(66, 130),
      v: "0x" + sig.slice(130, 132),
    };
  });

  it("should correctly recover signer", async () => {
    await expect(test.recoverWithSignature(1, sig))
      .to.emit(test, "Verification")
      .withArgs(signer.address);
  });

  it("should correctly recover signer from splitted signature", async () => {
    await expect(test.recoverWithSplittedSignature(1, sigSplitted))
      .to.emit(test, "Verification")
      .withArgs(signer.address);
  });
});

const getDomain = (chainId, address) => {
  return {
    name: "RecoverTest",
    version: "1.0",
    chainId: chainId,
    verifyingContract: address,
  };
};
