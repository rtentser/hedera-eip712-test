// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/utils/cryptography/EIP712.sol";

contract RecoverTest is EIP712 {
    using ECDSA for bytes32;

    bytes32 private constant TYPEHASH = keccak256("Params(uint256 i)");
    event Verification(address signer);

    struct Params {
        uint256 i;
    }

    struct Sig {
        uint8 v;
        bytes32 r;
        bytes32 s;
    }

    constructor() EIP712("RecoverTest", "1.0") {}

    function recoverWithSignature(uint256 i, bytes calldata sig) external {
        bytes32 hashStruct = keccak256(abi.encode(TYPEHASH, i));
        address signer = _hashTypedDataV4(hashStruct).recover(sig);

        emit Verification(signer);
    }

    function recoverWithSplittedSignature(
        uint256 i,
        Sig calldata sig
    ) external {
        bytes32 hashStruct = keccak256(abi.encode(TYPEHASH, Params(i)));
        address signer = _hashTypedDataV4(hashStruct).recover(
            sig.v,
            sig.r,
            sig.s
        );
        emit Verification(signer);
    }
}
