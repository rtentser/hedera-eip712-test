const hre = require("hardhat");
require("dotenv").config();

const type = { Params: [{ name: "i", type: "uint256" }] };

async function main() {
  let [signer] = await ethers.getSigners();
  let chainId = (await ethers.provider.getNetwork()).chainId;

  const test = await hre.ethers.getContractAt(
    "RecoverTest",
    process.env.TEST_ADDRESS
  );
  let sig = await signer.signTypedData(getDomain(chainId), type, {
    i: 1,
  });
  let sigSplitted = {
    r: sig.slice(0, 66),
    s: "0x" + sig.slice(66, 130),
    v: "0x" + sig.slice(130, 132),
  };
  await test.recoverWithSplittedSignature(1, sigSplitted);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});

const getDomain = (chainId) => {
  return {
    name: "RecoverTest",
    version: "1.0",
    chainId: chainId,
    verifyingContract: process.env.TEST_ADDRESS,
  };
};
