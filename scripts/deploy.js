const hre = require("hardhat");

async function main() {
  const test = await hre.ethers.deployContract("RecoverTest");

  await test.waitForDeployment();

  console.log(`Test deployed to ${await test.getAddress()}`);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
