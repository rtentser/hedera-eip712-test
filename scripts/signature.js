const hre = require("hardhat");
require("dotenv").config();

const type = { Params: [{ name: "i", type: "uint256" }] };

async function main() {
  let [signer] = await ethers.getSigners();
  let chainId = (await ethers.provider.getNetwork()).chainId;

  const test = await hre.ethers.getContractAt(
    "RecoverTest",
    process.env.TEST_ADDRESS
  );
  let sig = await signer.signTypedData(getDomain(chainId), type, {
    i: 1,
  });
  await test.recoverWithSignature(1, sig);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});

const getDomain = (chainId) => {
  return {
    name: "RecoverTest",
    version: "1.0",
    chainId: chainId,
    verifyingContract: process.env.TEST_ADDRESS,
  };
};
